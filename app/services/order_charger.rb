# frozen_string_literal: true

class OrderCharger
  def initialize(order)
    @order = order
  end

  # This is where the business logic related to charging customers need to stored and
  # this will only work if charging works without user input, like already stored and authorised cards
  # this service also responsible for storing transactions in payment_transactions table (currently does not exist)
  def charge
    # user.transactions.create(amount: total)
    # restaurant.transactions.create(amount: total) # FIXME: does service fee to be paid for restaurant?
  end
end
