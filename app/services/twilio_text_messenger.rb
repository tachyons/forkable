# frozen_string_literal: true

# This is just reference implementation, twilio is not configured in the project

class TwilioTextMessenger
  attr_reader :message, :receiver

  def initialize(message, receiver)
    @message = message
    @receiver = receiver
  end

  def call
    client = Twilio::REST::Client.new
    client.messages.create({
                             from: Rails.application.secrets.twilio_phone_number,
                             to: receiver,
                             body: message
                           })
  end
end
