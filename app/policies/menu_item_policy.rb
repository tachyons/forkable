# frozen_string_literal: true

class MenuItemPolicy < ApplicationPolicy
  def create?
    user&.restaurant&.menu == record.menu
  end

  def show?
    user&.restaurant&.menu == record.menu
  end

  def update?
    user&.restaurant&.menu == record.menu
  end
end
