# frozen_string_literal: true

class OrderMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_mailer.notify_user_order_ready.subject
  #
  def notify_user_order_ready
    @order = params[:order]
    @user = @order.user

    mail to: @user.email
  end
end
