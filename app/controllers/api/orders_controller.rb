# frozen_string_literal: true

module Api
  class OrdersController < ApplicationController
    before_action :authenticate_user
    def create
      order = current_user.orders.new
      menu_item = MenuItem.find(params[:menu_item_id])
      order.order_items.new(menu_item:, price: menu_item.price, item_name: menu_item.name)
      order.restaurant = menu_item.menu.restaurant
      order.save!
      order.set_order_total
      head :ok
    end

    def index
      @orders = if params[:previous] == 'true'
                  current_user.orders.delivered
                else
                  current_user.orders.where.not(state: 'delivered')
                end
    end

    def show
      @order = current_user.orders.find(params[:id])
    end
  end
end
