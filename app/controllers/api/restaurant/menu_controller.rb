# frozen_string_literal: true

module Api
  module Restaurant
    class MenuController < ApplicationController
      before_action :authenticate_restaurant_user

      def create
        menu = Menu.new(restaurant: current_restaurant, available: true)
        menu_params[:menu_items]&.each do |menu_item|
          menu.menu_items.build(price: menu_item[:price], name: menu_item[:name])
        end
        if menu.save
          head :ok
        else
          head :unprocessable_entity
        end
      end

      def index
        @menu = current_restaurant.menu
      end

      private

      def menu_params
        params.permit(:available, menu_items: %i[name price])
      end
    end
  end
end
