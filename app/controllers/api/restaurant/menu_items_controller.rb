# frozen_string_literal: true

module Api
  module Restaurant
    class MenuItemsController < ApplicationController
      before_action :authenticate_restaurant_user

      def create
        @menu = Menu.find(params[:menu_id])
        authorize @menu.menu_items.new
        @menu.menu_items.create(menu_item_params)
      end

      def show
        @menu_item = authorize MenuItem.find(params[:id])
      end

      def update
        @menu_item = MenuItem.find(params[:id])
        authorize @menu_item
        @menu_item.update(menu_item_params)
      end

      private

      def menu_item_params
        params.permit(:price, :name)
      end
    end
  end
end
