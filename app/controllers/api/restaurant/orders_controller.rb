# frozen_string_literal: true

module Api
  module Restaurant
    class OrdersController < ApplicationController
      before_action :authenticate_restaurant_user

      def update
        @order = current_restaurant.orders.find(params[:id])
        @order.update(order_params)
      end

      def index
        @orders = if params[:new_orders]
                    current_restaurant.orders.draft
                  else
                    current_restaurant.orders.all
                  end
      end

      def confirm
        @order = current_restaurant.orders.find(params[:id])
        if @order.confirm!
          head :ok
        else
          render json: { errors: @order.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def ready
        @order = current_restaurant.orders.find(params[:id])
        if @order.ready!
          head :ok
        else
          render json: { errors: @order.errors.full_messages }, status: :unprocessable_entity
        end
      end

      private

      def order_params
        params.permit(:delivery_fee)
      end
    end
  end
end
