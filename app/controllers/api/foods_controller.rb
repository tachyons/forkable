# frozen_string_literal: true

module Api
  class FoodsController < ApplicationController
    before_action :authenticate_user

    def index
      @foods = MenuItem
               .joins(:menu)
               .includes(menu: :restaurant)
               .where(menus: { available: true })
    end
  end
end
