# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Pundit::Authorization
  attr_reader :current_user, :current_restaurant

  rescue_from Pundit::NotAuthorizedError, with: :unauthorize

  private

  def authenticate_user
    user_token = request.headers['X-USER-TOKEN']
    if user_token
      @current_user = User.find_by(access_token: user_token)
      return unauthorize if @current_user.nil?
    else
      unauthorize
    end
  end

  def authenticate_restaurant_user
    restaurant_user_token = request.headers['X-RESTAURANT-USER-TOKEN']
    if restaurant_user_token
      @current_restaurant_user = RestaurantUser.find_by(access_token: restaurant_user_token)
      @current_restaurant = @current_restaurant_user&.restaurant

      return unauthorize if @current_restaurant.nil?
    else
      unauthorize
    end
  end

  def unauthorize
    head :unauthorized
    false
  end

  def pundit_user
    @current_user || @current_restaurant_user
  end
end
