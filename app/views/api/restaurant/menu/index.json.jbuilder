# frozen_string_literal: true

json.menu do
  json.array! @menu.menu_items.each do |menu_item|
    json.id menu_item.id
    json.name menu_item.name
    json.price menu_item.price
  end
end
