# frozen_string_literal: true

json.array! @foods.each do |food|
  json.id food.id
  json.name food.name
  json.price food.price
  json.restaurant food.menu.restaurant.name
end
