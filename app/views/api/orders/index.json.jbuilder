# frozen_string_literal: true

json.array! @orders.each do |order|
  json.id order.id
  json.delivery_fee order.delivery_fee
  json.service_fee order.service_fee
  json.total order.total
end
