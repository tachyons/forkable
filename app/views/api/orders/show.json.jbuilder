# frozen_string_literal: true

json.order do
  json.id @order.id
  json.delivery_fee @order.delivery_fee
  json.service_fee @order.service_fee
  json.total @order.total
  json.eta @order.eta
end
