# frozen_string_literal: true

class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :menu_item

  validates :item_name, presence: true
  validates :price, presence: true
end
