# frozen_string_literal: true

class Restaurant < ApplicationRecord
  validates :name, presence: true

  has_one :menu, dependent: :destroy
  has_many :menu_items, through: :menu
  has_many :orders, dependent: :destroy
end
