# frozen_string_literal: true

class Order < ApplicationRecord
  include AASM

  belongs_to :user
  belongs_to :restaurant

  has_many :order_items, dependent: :destroy

  aasm column: :state do
    state :draft, initial: true
    state :confirmed
    state :ready
    state :delivered
    state :cancelled

    event :confirm do
      transitions from: :draft, to: :confirmed
      after do
        charge_user
      end
    end

    event :ready do
      transitions from: :confirmed, to: :ready
      after do
        calculate_eta
        notify_user_order_ready
      end
    end
  end

  # This is a business logic and better placed in the service, keeping this in the model for the time being
  # to avoid un necessary abstractions at this stage
  def set_order_total
    self.item_total = order_items.sum(:price)
    self.delivery_fee = 10 # Setting a constant to ease the logic
    self.service_fee = item_total * 0.1 # 10%
    save
  end

  private

  def charge_user
    OrderCharger.new(self).charge
  end

  def calculate_eta
    eta = Time.current + 30.minutes # This is very basic logic, actual logic will depend on various other factors
    update(eta:)
  end

  def notify_user_order_ready
    # OrderMailer.with(order: self).notify_user_order_ready.deliver_now
    message = <<~SMS
      Hello #{user.name}, your order is now ready to pickup and will be delivered soon
    SMS
    TwilioTextMessenger.new(message, user.phone_number).call
  end
end
