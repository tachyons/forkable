# frozen_string_literal: true

class Menu < ApplicationRecord
  belongs_to :restaurant
  validates :available, presence: true

  has_many :menu_items, dependent: :destroy
end
