# frozen_string_literal: true

class RestaurantUser < ApplicationRecord
  belongs_to :restaurant
  has_secure_token :access_token
end
