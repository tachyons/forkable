# frozen_string_literal: true

class User < ApplicationRecord
  validates :phone_number, uniqueness: true
  validates :phone_number, presence: true

  has_many :orders, dependent: :destroy

  has_secure_token :access_token
end
