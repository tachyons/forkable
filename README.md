# Food Delivery

Simple food delivery system

```
bundle install
bundle exec rails server
```
## Running Tests

To run tests, run the following command

```bash
  bundle exec rspec
```


## Contributing

Contributions are always welcome!

See `contributing.md` for ways to get started.

Please adhere to this project's `code of conduct`.

## Limitations and future improvements

- standardise the api output format
- Store restaurant location and sort foods by proximity from user location
- paginate all end points
- Implement sign-in and sign-up
- Implement cart to add multiple order items to order
- Support payments with user input, Eg payment gateways like stripe
- Use jwt token for better handling tokens including revokation 
