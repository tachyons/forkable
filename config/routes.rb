Rails.application.routes.draw do
  namespace :api, defaults: { format: 'json' } do
    resources :foods, only: :index
    resources :orders, only: %i[create index show]
    namespace :restaurant, shallow: true do
      resources :menu, only: %i[create index] do
        resources :menu_items, only: %i[show update create]
      end
      resources :orders, only: %i[update index] do
        member do
          put :confirm
          put :ready
        end
      end
    end
  end
end
