# frozen_string_literal: true

module ApiHelper
  def authenticated_header(user)
    { 'X-USER-TOKEN': user.access_token }
  end

  def restaurent_authenticated_header(restaurant_user)
    { 'X-RESTAURANT-USER-TOKEN': restaurant_user.access_token }
  end
end
