# frozen_string_literal: true

FactoryBot.define do
  factory :restaurant_user do
    email { Faker::Internet.email }
    access_token { Faker::Alphanumeric.alpha }
    restaurant
  end
end
