# frozen_string_literal: true

FactoryBot.define do
  factory :order_item do
    order
    item_name { Faker::Food.dish }
    price { 1 }
    menu_item
  end
end
