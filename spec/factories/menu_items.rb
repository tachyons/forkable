# frozen_string_literal: true

FactoryBot.define do
  factory :menu_item do
    name { Faker::Food.dish }
    price { 1 }
    menu
  end
end
