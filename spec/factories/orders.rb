# frozen_string_literal: true

FactoryBot.define do
  factory :order do
    user
    restaurant
    item_total { 1 }
    delivery_fee { 1 }
    service_fee { 1 }
  end
end
