# frozen_string_literal: true

FactoryBot.define do
  factory :menu do
    available { true }
    restaurant
  end
end
