# frozen_string_literal: true

require 'rails_helper'

RSpec.describe "Api::Restaurant::Menu::MenuItems", type: :request do
  let(:restaurant_user) { create(:restaurant_user) }
  let(:headers) { restaurent_authenticated_header(restaurant_user) }
  let(:menu) { create(:menu, restaurant: restaurant_user.restaurant) }
  let(:menu_item) { create(:menu_item, menu:) }

  describe "POST /create" do
    it "returns http success" do
      post "/api/restaurant/menu/#{menu_item.menu.id}/menu_items", params: { name: 'Pizza', price: 10 },
                                                                   headers: headers

      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/api/restaurant/menu_items/#{menu_item.id}", headers: headers

      expect(response).to have_http_status(:success)
    end
  end

  describe "PUT /update" do
    it "returns http success" do
      put "/api/restaurant/menu_items/#{menu_item.id}", params: { name: 'updated name' }, headers: headers

      expect(response).to have_http_status(:success)

      expect(menu_item.reload.name).to be_eql('updated name')
    end
  end
end
