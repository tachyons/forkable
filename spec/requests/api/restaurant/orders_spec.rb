# frozen_string_literal: true

require 'rails_helper'

RSpec.describe "Api::Restaurant::Orders", type: :request do
  let(:restaurant_user) { create(:restaurant_user) }
  let(:headers) { restaurent_authenticated_header(restaurant_user) }
  let(:order) { create(:order, restaurant: restaurant_user.restaurant) }
  describe "PUT /update" do
    it "returns http success" do
      put "/api/restaurant/orders/#{order.id}", headers: headers, params: { delivery_fee: 10 }
      expect(response).to have_http_status(:success)
      expect(order.reload.delivery_fee).to be_eql(10)
    end
  end

  describe "GET /index" do
    it "returns http success" do
      get "/api/restaurant/orders", headers: headers
      expect(response).to have_http_status(:success)
    end
  end

  describe "PUT /confirm" do
    it "returns http success" do
      put "/api/restaurant/orders/#{order.id}/confirm", headers: headers
      expect(response).to have_http_status(:success)
      expect(order.reload).to be_confirmed
    end
  end

  describe "PUT /ready" do
    it "returns http success" do
      order.confirm!
      allow_any_instance_of(TwilioTextMessenger).to receive(:call).and_return(true) # the feature is incomplete
      put "/api/restaurant/orders/#{order.id}/ready", headers: headers
      expect(response).to have_http_status(:success)
      expect(order.reload).to be_ready
    end
  end
end
