# frozen_string_literal: true

require 'rails_helper'

RSpec.describe "Api::Restaurant::Menus", type: :request do
  let(:restaurant_user) { create(:restaurant_user) }
  let(:headers) { restaurent_authenticated_header(restaurant_user) }

  describe "POST /create" do
    it "returns http success" do
      post "/api/restaurant/menu", params: { available: true, menu_items: [
        { name: 'Pizza', price: 10 },
        { name: 'Pineapple Pizza', price: 1 }
      ] }, headers: headers
      expect(response).to have_http_status(:success)
      expect(restaurant_user.restaurant.menu).not_to be_nil
      expect(restaurant_user.restaurant.menu.menu_items.size).to be_eql(2)
    end
  end

  describe "GET /menu" do
    let(:restaurant) { restaurant_user.restaurant }
    let(:menu) { create(:menu, restaurant:) }
    let!(:menu_items) { create_list(:menu_item, 4, menu:) }

    it "returns http success" do
      get "/api/restaurant/menu", headers: headers
      expect(response).to have_http_status(:success)
      expect(response.parsed_body["menu"]).to include(a_hash_including("name" => menu_items.first.name))
    end
  end
end
