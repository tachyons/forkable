# frozen_string_literal: true

require 'rails_helper'

RSpec.describe "Api::Foods", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/api/foods", headers: authenticated_header(create(:user))
      expect(response).to have_http_status(:success)
      expect(response.parsed_body).to be_eql([])
    end

    it "returns menu items" do
      create_list(:menu_item, 5)
      get "/api/foods", headers: authenticated_header(create(:user))
      expect(response).to have_http_status(:success)
      expect(response.parsed_body.count).to be_eql(5)
      first_item = MenuItem.first
      expect(response.parsed_body.first).to be_eql({
                                                     'id' => first_item.id,
                                                     'name' => first_item.name,
                                                     'price' => first_item.price,
                                                     'restaurant' => first_item.menu.restaurant.name
                                                   })
    end
  end
end
