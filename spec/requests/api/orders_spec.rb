# frozen_string_literal: true

require 'rails_helper'

RSpec.describe "Api::Orders", type: :request do
  describe "POST /api/orders" do
    it "returns http success" do
      menu_item = create(:menu_item)
      post "/api/orders", headers: authenticated_header(create(:user)), params: { menu_item_id: menu_item.id }
      expect(response).to have_http_status(:success)
      expect(Order.last.order_items.first.item_name).to be_eql(menu_item.name)
    end
  end

  describe "GET /api/posts" do
    let(:user) { create(:user) }
    it "returns active orders" do
      create(:order, user:)
      get "/api/orders", headers: authenticated_header(user)
      expect(response).to have_http_status(:success)
      expect(response.parsed_body.length).to be_eql(1)
    end

    it "returns past orders" do
      create(:order, user:, state: 'delivered')
      create(:order, user:)
      get "/api/orders?previous=true", headers: authenticated_header(user)
      expect(response).to have_http_status(:success)
      expect(response.parsed_body.length).to be_eql(1)
    end
  end

  describe "GET /api/posts/:id" do
    let(:user) { create(:user) }
    let(:order) { create(:order, user:) }

    it "returns order details" do
      get "/api/orders/#{order.id}", headers: authenticated_header(user)
      expect(response).to have_http_status(:success)
      expect(response.parsed_body).to match({
                                              "order" => a_hash_including(
                                                "id" => order.id
                                              )
                                            })
    end
  end
end
