# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RestaurantUser, type: :model do
  it "Has valid factory" do
    expect(create(:restaurant_user)).to be_valid
  end
end
