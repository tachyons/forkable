# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Order, type: :model do
  it 'Has valid factory' do
    expect(create(:order)).to be_valid
  end
end
