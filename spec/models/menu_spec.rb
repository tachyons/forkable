# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Menu, type: :model do
  it 'Has valid factory' do
    expect(create(:menu)).to be_valid
  end
end
