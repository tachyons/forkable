# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MenuItem, type: :model do
  it 'Has valid factory' do
    expect(create(:menu_item)).to be_valid
  end
end
