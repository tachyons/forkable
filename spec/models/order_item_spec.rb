# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrderItem, type: :model do
  it 'Has valid factory' do
    expect(create(:order_item)).to be_valid
  end
end
