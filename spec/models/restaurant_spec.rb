# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Restaurant, type: :model do
  it 'Has valid factory' do
    expect(create(:restaurant)).to be_valid
  end
end
