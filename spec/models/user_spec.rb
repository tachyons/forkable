# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it 'Has valid factory' do
    expect(create(:user)).to be_valid
  end

  it "Sets the order total field" do
    order = create(:order)
    create(:order_item, price: 20, order:)
    create(:order_item, price: 40, order:)
    order.set_order_total
    order.reload
    expect(order.item_total).to be_eql(60)
    expect(order.delivery_fee).to be_eql(10)
    expect(order.service_fee).to be_eql(6)
    expect(order.total).to be_eql(76)
  end
end
