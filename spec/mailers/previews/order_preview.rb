# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/order
class OrderPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/order/notify_user_order_ready
  def notify_user_order_ready
    OrderMailer.notify_user_order_ready
  end
end
