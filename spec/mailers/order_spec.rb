# frozen_string_literal: true

require "rails_helper"

RSpec.describe OrderMailer, type: :mailer do
  describe "notify_user_order_ready" do
    let(:user) { create(:user) }
    let(:order) { create(:order, user:) }
    let(:mail) { OrderMailer.with(order:).notify_user_order_ready }

    it "renders the headers" do
      expect(mail.subject).to eq("Notify user order ready")
      expect(mail.to).to eq([user.email])
      expect(mail.from).to eq(["noreply@forkable.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Your order is now ready")
    end
  end
end
