# frozen_string_literal: true

class CreateMenus < ActiveRecord::Migration[7.0]
  def change
    create_table :menus do |t|
      t.boolean :available, null: false
      t.references :restaurant, null: false, foreign_key: true

      t.timestamps
    end
  end
end
