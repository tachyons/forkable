# frozen_string_literal: true

class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.references :user, null: false, foreign_key: true
      t.references :restaurant, null: false, foreign_key: true
      t.integer :item_total, null: false, default: 0
      t.integer :delivery_fee, null: false, default: 0
      t.integer :service_fee, null: false, default: 0
      t.virtual :total, type: :numeric, as: 'item_total + delivery_fee + service_fee', stored: true

      t.timestamps
    end
  end
end
