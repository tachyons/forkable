# frozen_string_literal: true

class AddStateAndEtaToOrders < ActiveRecord::Migration[7.0]
  def change
    add_column :orders, :state, :string, null: false, default: 'draft'
    add_column :orders, :eta, :datetime
  end
end
